package com.iiitd.covidapp.gt

import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class Suggestion : AppCompatActivity() {

    var singleton: Singleton = Singleton.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_suggestion)

        val txt_symptom_type = findViewById<TextView>(R.id.txt_symptom_type)
        val sb = StringBuilder()
        for (symptom in singleton.symptom) {
            sb.append(symptom)
            sb.append("\n")
        }

        txt_symptom_type.setText(sb)
        val size_of_sympton = singleton.symptom.size

        if(size_of_sympton > 3 ) {
            Toast.makeText(this@Suggestion, "RTPCR", Toast.LENGTH_SHORT).show()
        }else {
            Toast.makeText(this@Suggestion, "No RTPCR", Toast.LENGTH_SHORT).show()
        }
        // For Each Loop for iterating ArrayList
        // For Each Loop for iterating ArrayList
        for (symptom in singleton.symptom)  // Printing the elements of ArrayList
            Toast.makeText(this@Suggestion, symptom, Toast.LENGTH_SHORT).show()
        }//set on click klister
}
