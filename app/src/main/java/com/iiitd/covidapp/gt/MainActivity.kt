package com.iiitd.covidapp.gt

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class MainActivity : AppCompatActivity() {

    var singleton: Singleton = Singleton.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn_submit = findViewById<Button>(R.id.btn_submit)
        val switch_fever = findViewById<Switch>(R.id.switch_fever)
        val switch_runny_rose = findViewById<Switch>(R.id.switch_runny_rose)

        val local_symptom = ArrayList<String>()

        switch_fever?.setOnCheckedChangeListener({ _, isChecked ->
            val message = if (isChecked) "Fever : Yes" else "Fever : No"
            local_symptom.add(message)
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()

        })

        switch_runny_rose?.setOnCheckedChangeListener({ _, isChecked ->
            val message = if (isChecked) "Runny Nose : Yes" else "Runny Nose : No"
            local_symptom.add(message)
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
        })

        // transfer local variable to the global variable
        singleton.symptom = local_symptom

        // set on-click listener
        btn_submit.setOnClickListener {

            Toast.makeText(applicationContext, "Hello GT", Toast.LENGTH_LONG).show()
            val myIntent = Intent(this@MainActivity, Suggestion::class.java)
            startActivity(myIntent)
        }//set on click klister
    }
}