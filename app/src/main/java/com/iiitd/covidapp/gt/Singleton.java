package com.iiitd.covidapp.gt;

import java.util.ArrayList;

public class Singleton {

    private static Singleton singleton = new Singleton( );

    public ArrayList<String> symptom = null;

    /**
     *
     * A private Constructor prevents any other
     * class from instantiating.
     *
     */
    private Singleton(){ }

    /* Static 'instance' method */
    public static Singleton getInstance( ) {
        return singleton;
    }

}// end main singleton
